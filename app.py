# -*- coding: utf-8 -*-

#
# Copyright (C) 2015-2016 Rodrigo Ramírez Norambuena <a@rodrigoramirez.com>
#

from flask import Flask, render_template, request
from database import get_server_free
import subprocess


app = Flask(__name__)


def get_server(key, number=1):
    servers = []
    for i in range(0, number):
        server = get_server_free(key)
        if server is not None:
            servers.append(server.ip)
    return servers


@app.route('/add_key', methods=['POST'])
def add_key():
    key = request.form['key']
    servers = get_server(key, 2)
    print servers
    for server in servers:
        cmd = "echo '%s' | ssh -oStrictHostKeyChecking=no root@%s 'cat >> .ssh/authorized_keys'" % (key, server)
        subprocess.Popen(cmd, shell=True)
    return render_template('servers.html', servers=servers)


@app.route('/')
def home():
    return render_template('index.html')


def main():
    app.run(host='0.0.0.0', port=8080, use_reloader=True)

if __name__ == '__main__':
    main()
