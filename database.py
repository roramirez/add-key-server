# -*- coding: utf-8 -*-

#
# Copyright (C) 2015-2016 Rodrigo Ramírez Norambuena <a@rodrigoramirez.com>
#

from sqlalchemy import create_engine, MetaData
from sqlalchemy import Column, Integer, String, TIMESTAMP
from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm.exc import NoResultFound
import datetime

engine = create_engine('sqlite:///database.db', echo=True)

# session
session_db = scoped_session(sessionmaker(bind=engine,
                                         autoflush=False,
                                         autocommit=False))

DeclarativeBase = declarative_base()
metadata = MetaData()


class Server(DeclarativeBase):

    __tablename__ = "server"

    id = Column(Integer, primary_key=True)
    ip = Column(String)
    used_at = Column(TIMESTAMP)
    key = Column(String)

    def __init__(self, name):
        self.name = name


def get_server_free(key):
    try:
        q = session_db.query(Server)
        q = q.filter(Server.used_at == None)
        server = q.order_by(Server.id.asc()).limit(1).one()
        server.key = key
        server.used_at = datetime.datetime.now()
        session_db.add(server)
        session_db.commit()
        return server
    except NoResultFound, e:
        print(e)
        return None


DeclarativeBase.metadata.create_all(engine)
